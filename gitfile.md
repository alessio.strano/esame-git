# Comandi di Git
## git init

Il comando git init viene utilizzato per inizializzare un nuovo repository Git vuoto. Si crea una cartella .git nella directory corrente e si inizia a tenere traccia del repository.

## git clone

Il comando git clone viene utilizzato per clonare un repository Git esistente. Viene creata una copia identica del repository remoto nella directory corrente.

## git add

Il comando git add viene utilizzato per aggiungere file alla staging area. La staging area è una zona temporanea dove si possono preparare i file per il commit.

## git commit

Il comando git commit viene utilizzato per committare i file nella staging area. Si aggiunge un messaggio di commit che descrive le modifiche effettuate.
## git status

Il comando git status viene utilizzato per visualizzare lo stato attuale del repository. Mostra i file che sono stati modificati, quelli che sono stati aggiunti alla staging area e quelli che devono ancora essere committati.
## git log

Il comando git log viene utilizzato per visualizzare la cronologia dei commit. Mostra i dettagli di ogni commit, come l'autore, la data e l'ora, il messaggio di commit e il codice hash.
## git branch

Il comando git branch viene utilizzato per creare, visualizzare e gestire i branch. I branch sono delle copie isolate del repository dove si possono effettuare modifiche senza influire sul branch principale.
## git checkout

Il comando git checkout viene utilizzato per spostarsi tra i branch o le versioni dei file. Si può utilizzare per creare una nuova branch o per tornare ad una versione precedente dei file.
## git merge

Il comando git merge viene utilizzato per unire i branch. Si utilizza per integrare le modifiche effettuate in un branch con quelle di un altro.
## git pull

Il comando git pull viene utilizzato per scaricare e integrare le modifiche dal repository remoto nel repository locale.
## git push

Il comando git push viene utilizzato per caricare le modifiche effettuate nel repository locale sul repository remoto.
## git remote
Il comando git remote viene utilizzato per gestire i repository remoti. Si utilizza per aggiungere, rimuovere o visualizzare i repository remoti associati al repository locale.
## git fetch

Il comando git fetch viene utilizzato per scaricare i dati dal repository remoto senza fondere i branch. Si utilizza per aggiornare il repository locale con le ultime modifiche dal repository remoto.
## git tag

Il comando git tag viene utilizzato per etichettare i commit specifici. Si utilizza per marcare versioni specifiche del codice o per identificare particolari milestone.
## git revert

Il comando git revert viene utilizzato per annullare i commit precedenti. Si utilizza per eliminare le modifiche effettuate in un commit specifico senza eliminare il commit stesso.